/**
 * @file
 * taskController.
 *
 * Contain every request handler related to task resources.
 */

/**
 * Dependencies */
const { Task } = require("../models");

async function getTask(req, res) {
  const task = await Task.findOne({
    where: {
      id: req.params.id,
      userId: req.user?.id,
    },
  });

  if (!task)
    return res.status(404).json({
      status: "FAIL",
      data: {
        name: "TaskNotFoundError",
        message: `Task with id=${req.params.id} not found!`,
      },
    });

  return task;
}

function doesQueryParameterExist(query, key) {
  return !!query[key];
}

/**
 * GET /api/v1/tasks */
async function handleGetTasksRequest(req, res) {
  const whereObject = {
    userId: req.user?.id,
  };

  // Add query object accordingly
  if (doesQueryParameterExist("isCompleted"))
    whereObject.isCompleted = req.query.isCompleted;
  if (doesQueryParameterExist("completedAt"))
    whereObject.completedAt = req.query.completedAt;
  if (doesQueryParameterExist("content"))
    whereObject.content = {
      [Op.iLike]: req.query.content,
    };

  const tasks = await Task.findAll({
    where: whereObject,
  });

  res.status(200).json({
    status: "OK",
    data: {
      tasks,
    },
  });
}

/**
 * POST /api/v1/tasks */
async function handlePostTasksRequest(req, res) {
  const { content, dueAt } = req.body;

  console.log(req.body);

  const task = await Task.create({
    content,
    dueAt,
    userId: req.user?.id,
  });

  res.status(201).json({
    status: "OK",
    data: task,
  });
}

/**
 * GET /api/v1/tasks/:id */
async function handleGetTaskRequest(req, res) {
  const tasks = await Task.findOne({
    where: {
      id: req.params.id,
      userId: req.user?.id,
    },
  });

  res.status(200).json({
    status: "OK",
    data: task,
  });
}

/**
 * PUT /api/v1/tasks/:id */
async function handlePutTaskRequest(req, res) {
  const { content, dueAt, isCompleted, completedAt } = req.body;
  const task = await getTask(req, res);

  await task.update({
    content,
    dueAt,
    isCompleted,
    completedAt,
  });

  res.status(200).json({
    status: "OK",
    data: task,
  });
}

/**
 * DELETE /api/v1/tasks/:id */
async function handleDeleteTaskRequest(req, res) {
  const task = await getTask(req, res);

  await task.destroy();

  res.status(204).json({
    status: "OK",
    data: task,
  });
}

module.exports = {
  handleGetTasksRequest,
  handlePostTasksRequest,
  handleGetTaskRequest,
  handlePutTaskRequest,
  handleDeleteTaskRequest,
};
